﻿using System.Diagnostics;
using Xamarin.Forms;

namespace HelloCS481
{
    public partial class HelloCS481Page : ContentPage
    {
        int rotationIncrement = 10;

        public HelloCS481Page()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(HelloCS481Page)}");
            InitializeComponent();
        }

        void OnSpinItClicked(object sender, System.EventArgs e)
        {

            int newRotationValue = (int)WelcomeLabel.Rotation + rotationIncrement;
            if (newRotationValue >= 360)
            {
                newRotationValue = newRotationValue - 360;
            }

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSpinItClicked)}:  Rotating label to {newRotationValue} degrees.");
            WelcomeLabel.Rotation = newRotationValue;
        }
    }
}
